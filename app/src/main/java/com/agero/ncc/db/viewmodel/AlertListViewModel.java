package com.agero.ncc.db.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.db.entity.Alert;

import java.util.List;

import javax.inject.Inject;


public class AlertListViewModel extends AndroidViewModel {


    private final LiveData<List<Alert>> itemAndPersonList;
    @Inject
    NccDatabase nccDatabase;

    public AlertListViewModel(Application application) {
        super(application);
        NCCApplication.getContext().getComponent().inject(this);
        itemAndPersonList = nccDatabase.getAlertDao().getAlerts();
    }

    public LiveData<List<Alert>> getItemAndPersonList() {
        return itemAndPersonList;
    }
}
