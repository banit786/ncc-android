package com.agero.ncc.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import timber.log.Timber;

public class DateTimeUtils {

    /**
     * This method returns a string formatted as "h:mm a Today" if the passed date is today, otherwise a format of "h:mm a  MM/dd/yy"
     *
     * @param serverDate
     * @return
     */
    public static String getDisplayTimeStamp(Date serverDate) {

        Date today = new Date();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        int value = dateFormat1.format(serverDate).compareTo(dateFormat1.format(today));

        if (value == 0) {
            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a", Locale.US);
            timeFormat.setTimeZone(TimeZone.getDefault());
            return " Today, " + timeFormat.format(serverDate);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "h:mm a", Locale.US);
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(serverDate);
        }
    }

    public static String getDisplayTime(Date serverDate, String timeZone) {
        Date today = new Date();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        int value = dateFormat1.format(serverDate).compareTo(dateFormat1.format(today));

        if (value == 0) {
            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a zzz", Locale.US);
            if (TextUtils.isEmpty(timeZone)) {
                timeZone = Calendar.getInstance().getTimeZone().getID();
            }

            timeFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return timeFormat.format(serverDate);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "h:mm a zzz", Locale.US);

            if (TextUtils.isEmpty(timeZone)) {
                timeZone = Calendar.getInstance().getTimeZone().getID();
            }

            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

            return dateFormat.format(serverDate);
        }
    }

    public static String getDisplayJobStatus(Date serverDate, String timeZone) {

        Date today = new Date();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        int value = dateFormat1.format(serverDate).compareTo(dateFormat1.format(today));
        if (value == 0) {
            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a zzz", Locale.US);
            if (TextUtils.isEmpty(timeZone)) {
                timeZone = Calendar.getInstance().getTimeZone().getID();
            }

            timeFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return " Today, " + timeFormat.format(serverDate);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, MMM d hh:mm aaa zzz", Locale.US);
            if (TextUtils.isEmpty(timeZone)) {
                timeZone = Calendar.getInstance().getTimeZone().getID();
            }

            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return dateFormat.format(serverDate);
        }
    }

    public static String getDisplayTimeForAlertDetail(Date updatedDate, String timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, MMM d, hh:mm aaa zzz", Locale.US);

        if (TextUtils.isEmpty(timeZone)) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }

        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        return dateFormat.format(updatedDate);
    }

    public static String getDisplayTimeForHistory(Date updatedDate, String timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, MMM d, h:mm aaa zzz", Locale.US);
        if (TextUtils.isEmpty(timeZone)) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }

        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat.format(updatedDate);
    }

    public static String getDisplayForDate(String timeZone, Date updatedDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        if(!TextUtils.isEmpty(timeZone)){
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        }else {
            dateFormat.setTimeZone(TimeZone.getDefault());
        }
        return dateFormat.format(updatedDate);
    }

    public static String getDisplayForDate(Date updatedDate) {

        return getDisplayForDate(null,updatedDate);
    }

    public static Date parse(String input) throws java.text.ParseException {

        Date parsedDate = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (TextUtils.isEmpty(input) || input.length() < 19) {
            input = getUtcTimeFormat();
        }

        try {
            Timber.d("parse: " + input);
            input = input.substring(0, 19);
            parsedDate = df.parse(input);
        } catch (Exception e) {
            input = getUtcTimeFormat();
        }
        return parsedDate;

    }

    public static String getUtcTimeFormat() {
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return outputFmt.format(time);
    }

    public static boolean isToday(String statusDate) {
        boolean isToday = false;
        Calendar calendardate = Calendar.getInstance();
        calendardate.get(Calendar.MONTH);
        calendardate.get(Calendar.DAY_OF_MONTH);
        calendardate.get(Calendar.YEAR);
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        if (mSimpleDateFormat.format(calendardate.getTime()).equalsIgnoreCase(statusDate)) {
            return true;
        }
        return isToday;
    }

    public static String getReqCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", Locale.US);
        return sdf.format(calendar.getTime());
    }

    public static String getDisplayTimeForHistory(String updatedDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date startDate;
        try {
            startDate = df.parse(updatedDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "MMM d", Locale.US);
            return "Today, " + dateFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updatedDate;

    }

    public static String getDisplayOldTimeForHistory(String updatedDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date startDate;
        try {
            startDate = df.parse(updatedDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "MMM d, yyyy", Locale.US);
            return dateFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updatedDate;
    }

    /**
     * calculate ETA "HH:MM" format.
     *
     * @param mETAValue
     * @return
     */
    public static String calculateETA(int mETAValue) {

        int minutes = mETAValue % 60;
        int hours = mETAValue / 60;
        return String.format(Locale.getDefault(), "%02d:%02d", hours, minutes);

    }

    public static long getTimeDifference(String dispatchCreatedDateTime) {

        Date createdDateTime = new Date(), currentDateTime = new Date();
        try {
            createdDateTime = DateTimeUtils.parse(dispatchCreatedDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (currentDateTime.getTime() - createdDateTime.getTime()) / 1000;
    }

    public static long getTimeDifference(long dispatchCreatedDateTime, long diffTimeSec) {
        return dispatchCreatedDateTime - diffTimeSec;
    }

    public static String getEtaDateTime(String mEtaDateTime, int mQuotedEta, boolean isPending, String timeZone) throws ParseException {
        SimpleDateFormat mSimpleFormater = new SimpleDateFormat("d MMMM yyyy", Locale.US);
        SimpleDateFormat mSimpleTimeFormat = new SimpleDateFormat("hh:mm a zzz", Locale.US);
        Calendar mCurrentTime = Calendar.getInstance();
        Date date;
        date = DateTimeUtils.parse(mEtaDateTime);
        mCurrentTime.setTime(date);
        if (isPending) {
            mCurrentTime.add(Calendar.MINUTE, mQuotedEta);
        }


        if (TextUtils.isEmpty(timeZone)) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }

        mSimpleFormater.setTimeZone(TimeZone.getTimeZone(timeZone));
        mSimpleTimeFormat.setTimeZone(TimeZone.getTimeZone(timeZone));


        if (DateTimeUtils.isToday(DateTimeUtils.getDisplayForDate(timeZone,date))) {
            return "Today, " + mSimpleTimeFormat.format(mCurrentTime.getTime());
        } else {
            return mSimpleFormater.format(date) + " at " + mSimpleTimeFormat.format(mCurrentTime.getTime());
        }
    }

    public static String addMinutesToServerTime(String mEtaDateTime, int addedEta) {
        Calendar mCurrentTime = Calendar.getInstance();
        Date date;
        try {
            date = DateTimeUtils.parse(mEtaDateTime);
            mCurrentTime.setTime(date);
            mCurrentTime.add(Calendar.MINUTE, addedEta);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format((mCurrentTime.getTime()));

    }

//    public static String getDeviceTimeZone() {
//        String deviceTimezone = Calendar.getInstance().getTimeZone().getDisplayName();
//
//        String timeZone = "";
//        for (String spill :
//                deviceTimezone.split(" ")) {
//            timeZone += spill.charAt(0);
//        }
//        return timeZone;
//    }
}
