package com.agero.ncc.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.model.AuthTokenModel;
import com.agero.ncc.model.RefreshTokenRequest;
import com.agero.ncc.model.SigninResponseModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.zopim.android.sdk.api.Chat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.RejectedExecutionException;

import javax.inject.Inject;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.agero.nccsdk.NccSdk.getApplicationContext;

public class TokenManager {


    @Inject
    NccApi nccApi;
    @Inject
    NccDatabase nccDatabase;
    @Inject
    SharedPreferences mPrefs;

    public SharedPreferences.Editor mEditor;

    private static TokenManager mTokenManager;

    private ArrayList<TokenReponseListener> mTokenReponseListenerList = new ArrayList<>();

    private static SigninResponseModel mSigninResponseModel;

    private static int mRetryTokenUpdateCount = 0;

    private static int mRetrySignInFirebaseCount = 0;
    private static long mLastRefreshedTime = 0;

    private TokenManager() {

    }

    public static TokenManager getInstance() {
        if (mTokenManager == null) {
            mTokenManager = new TokenManager();
            NCCApplication.getContext().getComponent().inject(mTokenManager);
        }
        return mTokenManager;
    }

    public void signOut() {
        if (mEditor == null && mPrefs != null) {
            mEditor = mPrefs.edit();
        }
//        nccDatabase.getAlertDao().deleteAll();
        mEditor.remove(NccConstants.SIGNIN_VENDOR_ID).commit();
        mEditor.remove(NccConstants.SIGNIN_USER_ID).commit();
        mEditor.remove(NccConstants.SIGNIN_USER_NAME).commit();
        mEditor.remove(NccConstants.SIGNIN_USER_ROLE).commit();
        mEditor.remove(NccConstants.APIGEE_TOKEN).commit();
        mEditor.remove(NccConstants.FIREBASE_TOKEN).commit();
        mEditor.remove(NccConstants.APIGEE_TOKEN_EXPIRE).commit();
        mEditor.remove(NccConstants.FIREBASE_REFRESH_TOKEN).commit();

        FirebaseAuth.getInstance().signOut();
    }

    public interface TokenReponseListener {
        void onRefreshSuccess();

        void onRefreshFailure();
    }


    private boolean isTokenExpired(Object activity) {

        long expiryTime = mPrefs.getLong(NccConstants.APIGEE_TOKEN_EXPIRE, 0);


        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());


        Date expireDate = new Date(expiryTime);
        Timber.d("expiryTime: " + outputFmt.format(expireDate));

        Timber.d("currentTimeM: " + outputFmt.format(new Date(System.currentTimeMillis())));


        try {
            if (activity != null && BuildConfig.DEBUG) {
                HashMap<String, String> extraDatas = new HashMap<>();

                extraDatas.put("expiryTime", "" + expiryTime);
                extraDatas.put("currentTime", "" + System.currentTimeMillis());

                extraDatas.put("expiryTimeFormated", "" + outputFmt.format(expireDate));
                extraDatas.put("currentTimeFormated", "" + outputFmt.format(new Date(System.currentTimeMillis())));

                extraDatas.put("isTokenExpired", "" + (System.currentTimeMillis() >= expiryTime));

                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).mintlogEventExtraData("Token Refresh validation", extraDatas);
                } else if (activity instanceof WelcomeActivity) {
                    ((WelcomeActivity) activity).mintlogEventExtraData("Token Refresh validation", extraDatas);
                } else if (activity instanceof SparkLocationUpdateService) {
                    ((SparkLocationUpdateService) activity).mintlogEventExtraData("Token Refresh validation", extraDatas);
                }else if (activity instanceof ChatActivity) {
                    ((ChatActivity) activity).mintlogEventExtraData("Token Refresh validation", extraDatas);
                }
            }
        }catch (Exception e){
            return false;
        }

        return System.currentTimeMillis() >= expiryTime;
    }

    private boolean isUserSignedout() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        return user == null;
    }

    public void validateToken(Object activityParam, TokenReponseListener tokenReponseListener) {

        /*if(activityParam == null){
            if(!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID,""))){
                activityParam = new HomeActivity();
            }else{
                activityParam = new WelcomeActivity();
            }
        }*/

        final Object activity = activityParam;

        if (mEditor == null) {
            mEditor = mPrefs.edit();
        }
        mTokenReponseListenerList.add(tokenReponseListener);

        if (isTokenExpired(activity)) {
            if (mTokenReponseListenerList.size() == 1) {
                String refreshToken = mPrefs.getString(NccConstants.FIREBASE_REFRESH_TOKEN, "");

                if (!TextUtils.isEmpty(refreshToken)) {
                    RefreshTokenRequest request = new RefreshTokenRequest(refreshToken, getDeviceId());

                    if(activity != null) {
                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(request));
                        if (activity instanceof HomeActivity) {
                            ((HomeActivity) activity).mintlogEventExtraData("Token Refresh Request", extraDatas);
                        } else if (activity instanceof WelcomeActivity) {
                            ((WelcomeActivity) activity).mintlogEventExtraData("Token Refresh Request", extraDatas);
                        }
                        else if(activity instanceof SparkLocationUpdateService){
                            ((SparkLocationUpdateService) activity).mintlogEventExtraData("Token Refresh Request", extraDatas);
                        }
                        else if(activity instanceof ChatActivity){
                            ((ChatActivity) activity).mintlogEventExtraData("Token Refresh Request", extraDatas);
                        }
                    }

                    if (Utils.isNetworkAvailable()) {
                        nccApi.refreshToken(BuildConfig.APIGEE_URL + "/refresh-token", BuildConfig.APIGEE_API_KEY, request).enqueue(new Callback<SigninResponseModel>() {
                            @Override
                            public void onResponse(Call<SigninResponseModel> call, Response<SigninResponseModel> response) {
                                if (response != null && response.body() != null) {
                                    Log.d("TokenRefresh", response.body().toString());
                                    mSigninResponseModel = response.body();

                                    if(activity != null) {
                                        HashMap<String, String> extraDatas = new HashMap<>();
                                        extraDatas.put("json", new Gson().toJson(mSigninResponseModel));
                                        if (activity instanceof HomeActivity) {
                                            ((HomeActivity) activity).mintlogEventExtraData("Token Refresh Response", extraDatas);
                                        } else if (activity instanceof WelcomeActivity) {
                                            ((WelcomeActivity) activity).mintlogEventExtraData("Token Refresh Response", extraDatas);
                                        }
                                        else if(activity instanceof SparkLocationUpdateService){
                                            ((SparkLocationUpdateService) activity).mintlogEventExtraData("Token Refresh Response", extraDatas);
                                        }else if (activity instanceof ChatActivity) {
                                            ((ChatActivity) activity).mintlogEventExtraData("Token Refresh Response", extraDatas);
                                        }
                                    }

                                   /* mEditor.putString(NccConstants.APIGEE_TOKEN, responseModel.getId_token()).commit();

                                    mEditor.putString(NccConstants.FIREBASE_TOKEN, responseModel.getFirebase_token()).commit();
                                    long expiresIn = System.currentTimeMillis() + (responseModel.getExpires_in() * 1000);
                                    expiresIn = expiresIn - 1800000;
                                    mEditor.putLong(NccConstants.APIGEE_TOKEN_EXPIRE, expiresIn).commit();*/

                                    long expiresIn = System.currentTimeMillis() + (mSigninResponseModel.getExpires_in() * 1000);
                                    expiresIn = expiresIn - 600000;
                                    mLastRefreshedTime = System.currentTimeMillis();
                                    mRetrySignInFirebaseCount = 0;
                                    signinFirebase(activity, mSigninResponseModel.getFirebase_token(), expiresIn, mSigninResponseModel.getId_token());
                                } else {
                                    notifyAllFailure();
                                }

                            }

                            @Override
                            public void onFailure(Call<SigninResponseModel> call, Throwable t) {

                                notifyAllFailure();
                            }
                        });
                    } else {
                       /* UserError userError = new UserError();
                        userError.message = activity.getString(R.string.network_error_message);
                        activity.showError(Object.Companion.getERROR_SHOW_TYPE_DIALOG(), userError);*/
                        notifyAllFailure();
                    }
                }
            }
        } else {
            if (isUserSignedout()) {
                String firebaseToken = mPrefs.getString(NccConstants.FIREBASE_TOKEN, "");
                String apigeeToken = mPrefs.getString(NccConstants.APIGEE_TOKEN, "");
                long expiryTime = mPrefs.getLong(NccConstants.APIGEE_TOKEN_EXPIRE, 0);
                mRetrySignInFirebaseCount = 0;
                signinFirebase(activity, firebaseToken, expiryTime, apigeeToken);
            } else {
                notifyAllSuccess();
            }
        }
    }

    private void notifyAllFailure() {

        if (mTokenReponseListenerList != null && mTokenReponseListenerList.size() > 0) {
            mTokenReponseListenerList.get(0).onRefreshFailure();
            mTokenReponseListenerList.clear();
        }
    }

    private void notifyAllSuccess() {
        if (mTokenReponseListenerList != null) {
           /* for (TokenReponseListener tokenReponseListener :
                    mTokenReponseListenerList) {
                try {
                    tokenReponseListener.onRefreshSuccess();
                }catch (ConcurrentModificationException e){
                   e.printStackTrace();
                }
            }*/

            Iterator<TokenReponseListener> iter = mTokenReponseListenerList.iterator();
            while(iter.hasNext()){
                try {
                    iter.next().onRefreshSuccess();
                    iter.remove();
                }catch (ConcurrentModificationException e){
                    e.printStackTrace();
                }catch (RejectedExecutionException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            try {
                mTokenReponseListenerList.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void signinFirebase(Object Object, String firebaseToken, long expiryTime, TokenReponseListener tokenReponseListener, String apigeeToken) {
        mRetrySignInFirebaseCount = 0;
        mLastRefreshedTime = System.currentTimeMillis();
        mTokenReponseListenerList.add(tokenReponseListener);
        signinFirebase(Object, firebaseToken, expiryTime, apigeeToken);
    }

    private void signinFirebase(Object obj, String firebaseToken, long expiryTime, String apigeeToken) {
        mRetrySignInFirebaseCount++;
        if(mRetrySignInFirebaseCount < 4) {
            FirebaseAuth.getInstance().signInWithCustomToken(firebaseToken)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                if (user != null) {
                                    if(obj != null) {
                                        if (obj instanceof HomeActivity) {
                                            ((HomeActivity) obj).mintlogEventExtraData("Signin Firebase Success", null);
                                        } else if (obj instanceof WelcomeActivity) {
                                            ((WelcomeActivity) obj).mintlogEventExtraData("Signin Firebase Success", null);
                                        }
                                        else if(obj instanceof SparkLocationUpdateService){
                                            ((SparkLocationUpdateService) obj).mintlogEventExtraData("Signin Firebase Success", null);
                                        }
                                        else if (obj instanceof ChatActivity) {
                                            ((ChatActivity) obj).mintlogEventExtraData("Signin Firebase Success", null);
                                        }
                                    }

                                    updateTokenInFirebase(obj, firebaseToken, expiryTime, apigeeToken);
                                } else {
                                    if(obj != null) {
                                        if (obj instanceof HomeActivity) {
                                            ((HomeActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                        } else if (obj instanceof WelcomeActivity) {
                                            ((WelcomeActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                        }else if (obj instanceof SparkLocationUpdateService) {
                                            ((SparkLocationUpdateService) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                        }else if (obj instanceof ChatActivity) {
                                            ((ChatActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                        }
                                    }
                                    signinFirebase(obj, firebaseToken, expiryTime, apigeeToken);
                                }
                            } else {
                                // If sign in fails, display a message to the user.
                                if(obj != null) {
                                    if (obj instanceof HomeActivity) {
                                        ((HomeActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                    } else if (obj instanceof WelcomeActivity) {
                                        ((WelcomeActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                    }else if (obj instanceof SparkLocationUpdateService) {
                                        ((SparkLocationUpdateService) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                    } else if (obj instanceof ChatActivity) {
                                        ((ChatActivity) obj).mintlogEventExtraData("Signin Firebase Retry " + mRetrySignInFirebaseCount, null);
                                    }
                                }
                                signinFirebase(obj, firebaseToken, expiryTime, apigeeToken);
                            }
                        }
                    });
        }else{
            if(obj != null) {
                if (obj instanceof HomeActivity) {
                    ((HomeActivity) obj).mintlogEventExtraData("Signin Firebase Failed", null);
                } else if (obj instanceof WelcomeActivity) {
                    ((WelcomeActivity) obj).mintlogEventExtraData("Signin Firebase Failed", null);
                }else if (obj instanceof SparkLocationUpdateService) {
                    ((SparkLocationUpdateService) obj).mintlogEventExtraData("Signin Firebase Failed", null);
                }else if (obj instanceof ChatActivity) {
                    ((ChatActivity) obj).mintlogEventExtraData("Signin Firebase Failed", null);
                }
            }
            notifyAllFailure();
        }
    }

    private void updateTokenInFirebase(Object object, String token, long expiretime, String apigeeToken) {
        String fcmId = Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/authtoken/" + fcmId);
        //reference.keepSynced(true);

        Date expireDate = new Date(expiretime);
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        AuthTokenModel tokenModel = new AuthTokenModel("Android", apigeeToken, outputFmt.format(expireDate),outputFmt.format(new Date(mLastRefreshedTime)));

        Timber.d("expiryTime: " + outputFmt.format(expireDate));


        mRetryTokenUpdateCount = 0;

        updateTokenInFirebaseWithRetry(object,reference, tokenModel);
    }

    private void updateTokenInFirebaseWithRetry(Object object,final DatabaseReference reference, final AuthTokenModel tokenModel) {
        mRetryTokenUpdateCount++;
        if (mRetryTokenUpdateCount < 4) {

            RxFirebaseDatabase.setValue(reference, tokenModel)
                    .subscribe(new Action() {
                        @Override
                        public void run() {
                            if(object != null) {
                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("json", new Gson().toJson(tokenModel));
                                if (object instanceof HomeActivity) {
                                    ((HomeActivity) object).mintlogEventExtraData("Update Token Firebase Success", extraDatas);
                                } else if (object instanceof WelcomeActivity) {
                                    ((WelcomeActivity) object).mintlogEventExtraData("Update Token Firebase Success", extraDatas);
                                }else if(object instanceof SparkLocationUpdateService){
                                    ((SparkLocationUpdateService) object).mintlogEventExtraData("Update Token Firebase Success", extraDatas);
                                }else if (object instanceof ChatActivity) {
                                    ((ChatActivity) object).mintlogEventExtraData("Update Token Firebase Success", extraDatas);
                                }
                            }
                            if (mSigninResponseModel != null) {
                                mEditor.putString(NccConstants.APIGEE_TOKEN, mSigninResponseModel.getId_token()).commit();
                                mEditor.putString(NccConstants.FIREBASE_TOKEN, mSigninResponseModel.getFirebase_token()).commit();
                                long expiresIn = System.currentTimeMillis() + (mSigninResponseModel.getExpires_in() * 1000);
                                expiresIn = expiresIn - 600000;

                                mEditor.putLong(NccConstants.APIGEE_TOKEN_EXPIRE, expiresIn).commit();
                                mSigninResponseModel = null;
                            }
                            notifyAllSuccess();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) {

                            if(object != null) {
                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("json", new Gson().toJson(tokenModel));

                                if (object instanceof HomeActivity) {
                                    ((HomeActivity) object).mintlogEventExtraData("Update Token Firebase Retry " + mRetryTokenUpdateCount, extraDatas);
                                } else if (object instanceof WelcomeActivity) {
                                    ((WelcomeActivity) object).mintlogEventExtraData("Update Token Firebase Retry" + mRetryTokenUpdateCount, extraDatas);
                                }else if(object instanceof SparkLocationUpdateService){
                                    ((SparkLocationUpdateService) object).mintlogEventExtraData("Update Token Firebase Retry" + mRetryTokenUpdateCount, extraDatas);
                                }else if (object instanceof ChatActivity) {
                                    ((ChatActivity) object).mintlogEventExtraData("Update Token Firebase Retry" + mRetryTokenUpdateCount, extraDatas);
                                }
                            }
                            updateTokenInFirebaseWithRetry(object,reference, tokenModel);
                        }
                    });
        } else {
            if(object != null) {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(tokenModel));
                if (object instanceof HomeActivity) {
                    ((HomeActivity) object).mintlogEventExtraData("Update Token Firebase Failure", extraDatas);
                } else if (object instanceof WelcomeActivity) {
                    ((WelcomeActivity) object).mintlogEventExtraData("Update Token Firebase Failure", extraDatas);
                }else if(object instanceof SparkLocationUpdateService){
                    ((SparkLocationUpdateService) object).mintlogEventExtraData("Update Token Firebase Failure", extraDatas);
                }else if (object instanceof ChatActivity) {
                    ((ChatActivity) object).mintlogEventExtraData("Update Token Firebase Failure", extraDatas);
                }
            }
            notifyAllFailure();
        }
    }


    public String getDeviceId(){
        if(!mPrefs.contains(NccConstants.DEVICE_ID)){
            mPrefs.edit().putString(NccConstants.DEVICE_ID, Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID)).commit();
        }
        return mPrefs.getString(NccConstants.DEVICE_ID,"");
    }


}
