package com.agero.ncc.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;

/**
 * Adapter for HistoryFragment.
 */
public class HistorySearchAdapter extends RecyclerView.Adapter<HistorySearchAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<JobDetail> mHistoryList;
    private int mSelectedJobPosition;

    public HistorySearchAdapter(Context mContext, ArrayList<JobDetail> mHistoryList, int selectedJobPosition) {
        this.mContext = mContext;
        this.mHistoryList = mHistoryList;
        this.mSelectedJobPosition = selectedJobPosition;
    }

    public void clear() {
        if (mHistoryList != null) {
            mHistoryList.clear();
        }
        mSelectedJobPosition = 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.history_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        if (mHistoryList.size() > holder.getAdapterPosition()) {
            JobDetail job = mHistoryList.get(holder.getAdapterPosition());
//        if (mHistoryList.size() - 1 == position) {
//            holder.mViewHistoryBorder.setVisibility(View.INVISIBLE);
//        }else{
//            holder.mViewHistoryBorder.setVisibility(View.VISIBLE);
//        }
            if (mContext.getResources().getBoolean(R.bool.isTablet)) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSelectedJobPosition = position;
                        notifyDataSetChanged();
                    }
                });

                if (mSelectedJobPosition == holder.getAdapterPosition()) {
                    holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.tab_tap_color));
                } else {
                    holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.ncc_white));
                }
            }
            try {
                if (job != null && job.getDispatchId() != null) {
                    holder.mJobId.setText(UiUtils.getJobIdDisplayFormat(job.getDispatchId()));
                }
                holder.mTextHistoryTow.setText(Utils.getDisplayServiceTypes(mContext, job.getServiceTypeCode()) + " - " + job.getVehicle().getYear()
                        + " " + job.getVehicle().getMake() + " " + job.getVehicle().getModel() + " " + job.getVehicle().getColor());
//            if(Utils.isTow(job.getServiceTypeCode())){
//                holder.mTextAddressHistory.setText(Utils.toCamelCase(job.getTowLocation().getAddressLine1() + ", "
//                        + job.getTowLocation().getCity() + ", " )+ job.getTowLocation().getState() + " "+job.getTowLocation().getPostalCode());
//            }else{
                holder.mTextAddressHistory.setText(Utils.toCamelCase(job.getDisablementLocation().getAddressLine1() + ", "
                        + job.getDisablementLocation().getCity() + ", ") + job.getDisablementLocation().getState() + " " + job.getDisablementLocation().getPostalCode());
//            }

                try {
                    if (job.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
                        holder.mTextHistoryEta.setText(mContext.getResources().getString(R.string.history_text_completed) + " " + mContext.getString(R.string.dot) + " "
                                + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
                    } else {

                        if (job.getCurrentStatus().getIsPending() != null && job.getCurrentStatus().getIsPending() && !job.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) {
                            holder.mTextHistoryEta.setText(mContext.getString(R.string.text_pending) + " " + Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()) + "... " + mContext.getString(R.string.dot) + " "
                                    + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
//                        holder.mTextHistoryEta.setTextColor(mContext.getResources()
//                                .getColor(R.color.jobdetail_pending_color));
                        } else {
                            holder.mTextHistoryEta.setText(Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()) + " " + mContext.getString(R.string.dot) + " "
                                    + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
//                        holder.mTextHistoryEta.setTextColor(mContext.getResources()
//                                .getColor(R.color.jobdetail_heading_color));
                        }


                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mHistoryList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_history_tow)
        TextView mTextHistoryTow;
        @BindView(R.id.text_address_history)
        TextView mTextAddressHistory;
        @BindView(R.id.text_history_eta)
        TextView mTextHistoryEta;
        @BindView(R.id.view_history_border)
        View mViewHistoryBorder;
        @BindView(R.id.text_active_job_id)
        TextView mJobId;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
