package com.agero.ncc.fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.AuthTokenModel;
import com.agero.ncc.model.CreateUserRequest;
import com.agero.ncc.model.CreateUserResponse;
import com.agero.ncc.model.SigninResponseModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.agero.nccsdk.NccSdk.getApplicationContext;

public class CreateAccountFragment extends CameraBaseFragment {


    @BindView(R.id.edit_verification_code)
    EditText mEditVerificationCode;
    @BindView(R.id.edit_first_name)
    EditText mEditFirstName;
    @BindView(R.id.edit_last_name)
    EditText mEditLastName;
    @BindView(R.id.edit_contact_number)
    EditText mEditContactNumber;
    @BindView(R.id.edit_password)
    EditText mEditPassword;
    @BindView(R.id.edit_confirm_password)
    EditText mEditConfirmPassword;
    @BindView(R.id.image_button_camera)
    CircleImageView mImageButtonUploadPhoto;
    @BindView(R.id.button_create_account)
    Button mButtonCreateAccount;

    /*  @BindView(R.id.text_terms_conditions)
      TextView mTextTermsConditions;*/
    @BindView(R.id.text_input_contact_number)
    TextInputLayout mTextInputContactNumber;
    @BindView(R.id.text_input_password)
    TextInputLayout mTextInputPassword;
    @BindView(R.id.text_input_confirm_password)
    TextInputLayout mTextInputConfirmPassword;
    @BindView(R.id.text_input_first_name)
    TextInputLayout mTextInputFirstName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout mTextInputLastName;
    @BindView(R.id.text_input_verification_code)
    TextInputLayout mTextInputVerificationCode;
    @BindView(R.id.edit_email)
    EditText mEditEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout mTextInputEmail;
    @BindView(R.id.image_account_back_arrow)
    ImageView mImageAccountBackArrow;
    @BindView(R.id.text_contact_number)
    TextView mTextContactNumber;
    @BindView(R.id.text_password_rule)
    TextView mTextPasswordRule;
    UserError mUserError;
    boolean isContactNumber, isCreatePassword;

    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.CREATE_ACCOUNT;


    @Inject
    NccApi nccApi;

    boolean isImageAvailable;
    MaskedFormatter formatter;
    String invitationCode;

    public static int INPUT_ERROR_RED = 2;
    public static int INPUT_ERROR_GRAY = 1;
    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            enableConfirmButton();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    private void hintEnabled(String mEditable, TextInputLayout mTextInputLayout) {
        if (mEditable.isEmpty()) {
            mTextInputLayout.setHintEnabled(false);
        } else {
            mTextInputLayout.setErrorEnabled(false);
        }
    }

    //  PhoneNumberUtil phoneUtil;
    @BindView(R.id.text_image_description)
    TextView mTextImageDescription;

    public CreateAccountFragment() {
        // Intentionally empty
    }

    public static CreateAccountFragment newInstance(String invitationCode) {
        CreateAccountFragment fragment = new CreateAccountFragment();
        Bundle args = new Bundle();
        args.putString("invitationCode", invitationCode);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_create_account, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        mWelcomeActivity = (WelcomeActivity) getActivity();
        mUserError = new UserError();
//        mWelcomeActivity.hideBottomBar();
        Toolbar toolbar = (Toolbar) mWelcomeActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        mEditVerificationCode.addTextChangedListener(mTextWatcher);
        mEditFirstName.addTextChangedListener(mTextWatcher);
        mEditLastName.addTextChangedListener(mTextWatcher);
        mEditContactNumber.addTextChangedListener(mTextWatcher);
        mEditPassword.addTextChangedListener(mTextWatcher);
        mEditConfirmPassword.addTextChangedListener(mTextWatcher);
        mEditEmail.addTextChangedListener(mTextWatcher);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            invitationCode = bundle.getString("invitationCode");
            mEditVerificationCode.setText(invitationCode);
        }
        mButtonCreateAccount.setBackgroundResource(R.drawable.border_corner_history);

        GradientDrawable drawableJobDetail = (GradientDrawable) mButtonCreateAccount.getBackground();
        drawableJobDetail.setColor(getResources().getColor(R.color.createacc_button_disable_color));

        formatter = new MaskedFormatter("(###) ###-####");
        mEditContactNumber.addTextChangedListener(new MaskedWatcher(formatter, mEditContactNumber));

        mEditEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                mTextInputEmail.setEnabled(true);
                mTextInputEmail.setErrorEnabled(false);
            }
        });
        mEditConfirmPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    proceedCreateAccount();
                }
                return false;
            }
        });
        mEditVerificationCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                validEmail();
                mTextInputVerificationCode.setEnabled(true);
                mTextInputVerificationCode.setErrorEnabled(false);
            }
        });

        mEditFirstName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                validEmail();
                validateFields(mTextInputVerificationCode, getResources().getString(R.string.createacc_error_verification_code), mEditVerificationCode.getText().toString());
                mTextInputFirstName.setEnabled(true);
                mTextInputFirstName.setErrorEnabled(false);
            }
        });

        mEditLastName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                validEmail();
                validateFields(mTextInputVerificationCode, getResources().getString(R.string.createacc_error_verification_code), mEditVerificationCode.getText().toString());
                validateFields(mTextInputFirstName, getResources().getString(R.string.createacc_error_firstname), mEditFirstName.getText().toString());
                mTextInputLastName.setEnabled(true);
                mTextInputLastName.setErrorEnabled(false);
//                if (!isContactNumber) {
//                    checkVersionCompatability(mEditContactNumber,R.color.ncc_editLineColor);
//                    mTextContactNumber.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
//                } else {
//                    checkVersionCompatability(mEditContactNumber,R.color.createacc_error_color);
//                }
            }
        });
//
//
        mEditContactNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                isContactNumber = true;
                validEmail();
                validateFields(mTextInputVerificationCode, getResources().getString(R.string.createacc_error_verification_code), mEditVerificationCode.getText().toString());
                validateFields(mTextInputFirstName, getResources().getString(R.string.createacc_error_firstname), mEditFirstName.getText().toString());
                validateFields(mTextInputLastName, getResources().getString(R.string.createacc_error_lastname), mEditLastName.getText().toString());
                mTextContactNumber.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                errorValidation(isContactNumber,mEditContactNumber,R.color.ncc_text_disabled_color);
//                if (!isCreatePassword) {
//                    checkVersionCompatability(mEditPassword,R.color.ncc_editLineColor);
//                    mTextPasswordRule.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
//                } else {
//                    checkVersionCompatability(mEditPassword,R.color.createacc_error_color);
//                }
                isContactNumber = false;
            }
        });
        mEditPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                isCreatePassword = true;
                validEmail();
                mTextPasswordRule.setEnabled(true);
                validateFields(mTextInputVerificationCode, getResources().getString(R.string.createacc_error_verification_code), mEditVerificationCode.getText().toString());
                validateFields(mTextInputFirstName, getResources().getString(R.string.createacc_error_firstname), mEditFirstName.getText().toString());
                validateFields(mTextInputLastName, getResources().getString(R.string.createacc_error_lastname), mEditLastName.getText().toString());
                contactNumberValidation();
                mTextPasswordRule.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                errorValidation(isCreatePassword,mEditPassword,R.color.ncc_text_disabled_color);
                isCreatePassword = false;
            }
        });
        mEditConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                validEmail();
                if (hasFocus) {
                    mTextInputConfirmPassword.setEnabled(true);
                    mTextInputConfirmPassword.setErrorEnabled(false);
                } else {
                    if (mEditConfirmPassword.getText().toString().isEmpty()) {
                        mTextInputConfirmPassword.setError(getString(R.string.createacc_error_password));
                    } else if (!mEditConfirmPassword.getText().toString().isEmpty() && !mEditPassword.getText().toString().equals(mEditConfirmPassword.getText().toString())) {
                        mTextInputConfirmPassword.setError(getString(R.string.createacc_error_password_donot_match));
                    } else {
                        mTextInputConfirmPassword.setErrorEnabled(false);
                        mTextInputConfirmPassword.setHintEnabled(false);
                    }
                }
                validateFields(mTextInputVerificationCode, getResources().getString(R.string.createacc_error_verification_code), mEditVerificationCode.getText().toString());
                validateFields(mTextInputFirstName, getResources().getString(R.string.createacc_error_firstname), mEditFirstName.getText().toString());
                validateFields(mTextInputLastName, getResources().getString(R.string.createacc_error_lastname), mEditLastName.getText().toString());
                contactNumberValidation();
                if (Utils.isValidPassword(mEditPassword.getText().toString())) {
                    mTextPasswordRule.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                } else {
                    checkVersionCompatability(mEditPassword, R.color.createacc_error_color);
                    mTextPasswordRule.setTextColor(getResources().getColor(R.color.createacc_error_color));
                }
            }
        });
        return superView;
    }

    private void proceedCreateAccount() {
        if (Utils.isNetworkAvailable()) {
            createAccount();
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    public void errorValidation(boolean b,EditText editText,int color) {
    if (b) {
        checkVersionCompatability(editText,color);
    } else {
        checkVersionCompatability(editText,color);
    }
}

    public void contactNumberValidation() {
        String text = mEditContactNumber.getText().toString();
        String unmaskedString = "";
        if(!TextUtils.isEmpty(text)) {
            unmaskedString = formatter.formatString(text).getUnMaskedString();
        }

        if (unmaskedString.isEmpty() || unmaskedString.length() < 10) {
            checkVersionCompatability(mEditContactNumber,R.color.createacc_error_color);
            mTextContactNumber.setTextColor(getResources().getColor(R.color.createacc_error_color));
        } else {
            mTextContactNumber.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));

        }
    }

    public void checkVersionCompatability(EditText editText,int color){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            editText.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        }else{
            editText.getBackground().mutate().setColorFilter(ContextCompat.getColor(getActivity(),color), PorterDuff.Mode.SRC_ATOP);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                mImageButtonUploadPhoto.setImageURI(Uri.parse(onSelectFromGalleryResult(data)));
            } else if (requestCode == REQUEST_CAMERA) {
                mImageButtonUploadPhoto.setImageURI(onCaptureImageResult(data));
            }
            isImageAvailable = mPrefs.getBoolean(NccConstants.ISIMAGEAVAILABLE, true);
        }
    }

    public void enableConfirmButton() {
        if (isValidEmail(mEditEmail.getText().toString())
                && !mEditVerificationCode.getText().toString().isEmpty()
                && !mEditFirstName.getText().toString().isEmpty()
                && !mEditLastName.getText().toString().isEmpty()
                && !mEditContactNumber.getText().toString().isEmpty()
                && mEditContactNumber.getText().length() == 14
                //&& isValidPassword(mEditPassword.getText().toString())
                && !mEditConfirmPassword.getText().toString().isEmpty()
                && mEditPassword.getText().toString().equals(mEditConfirmPassword.getText().toString())
            // && isValidPassword(mEditConfirmPassword.getText().toString())
                ) {
            mButtonCreateAccount.setEnabled(true);
            checkCorner();
            mButtonCreateAccount.setTextColor(getResources().getColor(R.color.ncc_white));
        } else {
            mButtonCreateAccount.setEnabled(false);
            checkCorner();
            mButtonCreateAccount.setTextColor(getResources().getColor(R.color.createacc_text_button_disbale_color));
        }
    }

public void checkCorner() {
    mButtonCreateAccount.setBackgroundResource(R.drawable.border_corner_history);
    GradientDrawable drawableCorner = (GradientDrawable) mButtonCreateAccount.getBackground();
    if(mButtonCreateAccount.isEnabled()) {
        drawableCorner.setColor(getResources().getColor(R.color.ncc_blue));
    } else{
        drawableCorner.setColor(getResources().getColor(R.color.createacc_button_disable_color));
    }
}
    public void validateFields(TextInputLayout textValid, String validateFields, String editText) {
        if (!editText.isEmpty() && !validateFields.isEmpty()) {
            textValid.setHintEnabled(false);
            textValid.setErrorEnabled(false);
            enableConfirmButton();
        } else {
            textValid.setErrorEnabled(true);
            textValid.setError(validateFields);
            textValid.setHintEnabled(false);
        }
    }

    private void termsAlertDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml(getString(R.string.terms_alert_title)), Html.fromHtml(getString(R.string.terms_alert_desc))
                , Html.fromHtml("<b>" + getString(R.string.terms_alert_agree) + "</b>"), Html.fromHtml("<b>" + getString(R.string.terms_alert_view) + "</b>"));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(isAdded()) {
                    if (!mWelcomeActivity.isFinishing()) {
                        if (i == -1) {
                            ((WelcomeActivity) mWelcomeActivity).pushFragment(EditProfileFragment.newInstance());
                        } else {
                            ((WelcomeActivity) mWelcomeActivity).pushFragment(EditProfileFragment.newInstance());
                        }
                    }
                    dialogInterface.dismiss();
                }
            }
        });
        alert.show(getFragmentManager().beginTransaction(), CreateAccountFragment.class.getClass().getCanonicalName());
    }

    private void verificationCodeAlertDialog() {
        if(isAdded() && !mWelcomeActivity.isFinishing()){
            AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml("<big>" + getResources().getString(R.string.verification_code_alert_title) + "</big>")
                    , Html.fromHtml("<font color=\"#757575\">" + getString(R.string.verification_code_alert_text) + "</font>"), "", Html.fromHtml("<b>" + getString(R.string.verification_code_alert_try_again) + "</b>"));

            alert.setListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (isAdded()) {
                        dialogInterface.dismiss();
                        mEditVerificationCode.requestFocus();
                        if (mEditVerificationCode.getText().toString().equals("")) {
                            mTextInputVerificationCode.setErrorEnabled(true);
                            mTextInputVerificationCode.setError(getResources().getString(R.string.createacc_error_verification_code));
                        }
                    }
                }
            });
            alert.show(getFragmentManager().beginTransaction(), CreateAccountFragment.class.getClass().getCanonicalName());
        }
    }

    @OnClick({R.id.image_button_camera, R.id.text_image_description, R.id.button_create_account, R.id.image_account_back_arrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_button_camera:
                showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_CREATE_ACCOUNT);
                break;
            case R.id.text_image_description:
                showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_CREATE_ACCOUNT);
                break;
            case R.id.button_create_account:
               proceedCreateAccount();
               eventAction = NccConstants.FirebaseEventAction.CONTINUE;
               mWelcomeActivity.firebaseLogEvent(eventCategory,eventAction);

//                mWelcomeActivity.push(new MobileSignInFragment());
                break;
            case R.id.image_account_back_arrow:
                if(isAdded()){
                    getActivity().onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isImageAvailable) {
            mTextImageDescription.setText(R.string.createacc_text_add_photo);
        } else {
            mTextImageDescription.setText(R.string.createacc_text_replace_photo);
        }

    }

    private void showSelectRoleDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog("Choose Role"
                , "Login as Driver or Dispatcher", "Dispatcher", "Driver");

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(isAdded()) {
                    ArrayList<String> roleList = new ArrayList<>();
                    if (i == DialogInterface.BUTTON_POSITIVE) {
                        roleList.add(NccConstants.USER_ROLE_DISPATCHER);
                    } else if (i == DialogInterface.BUTTON_NEGATIVE) {
                        roleList.add(NccConstants.USER_ROLE_DRIVER);
                    }
                    mEditor.putString(NccConstants.SIGNIN_USER_ROLE, new Gson().toJson(roleList)).commit();
                    dialogInterface.dismiss();
                    if (!mWelcomeActivity.isFinishing() && mWelcomeActivity instanceof WelcomeActivity) {
                        ((WelcomeActivity) mWelcomeActivity).pushFragment(TermsAndConditionsFragment.newInstance());
                    }
                }
//                termsAlertDialog();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), CreateAccountFragment.class.getClass().getCanonicalName());
    }

    public void setContactNumberHint(int type) {
        if (type == INPUT_ERROR_GRAY) {
//            mTextInputContactNumber.setError(getResources().getString(R.string.createacc_error_contact_number));
//            mTextInputContactNumber.setErrorTextAppearance(R.style.text_input_style);
        } else {
//            mTextInputContactNumber.setError(getResources().getString(R.string.createacc_error_contact_number));
//            mTextInputContactNumber.setErrorTextAppearance(R.style.error_style);

        }

    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void validEmail() {
        if (isValidEmail(mEditEmail.getText().toString().trim())) {
            mTextInputEmail.setEnabled(true);
            mTextInputEmail.setErrorEnabled(false);
        } else {
            mTextInputEmail.setErrorEnabled(true);
            if (!mEditEmail.getText().toString().trim().isEmpty()) {
                mTextInputEmail.setError(getResources().getString(R.string.createacc_error_email_format));
            } else {
                mTextInputEmail.setError(getResources().getString(R.string.createacc_error_email));
            }
        }
    }

    private void createAccount() {
        showProgress();
        String firstName = mEditFirstName.getText().toString();
        String lastName = mEditLastName.getText().toString();
        String password = mEditPassword.getText().toString();
        String emailaddress = "";
        if(!TextUtils.isEmpty(mEditEmail.getText().toString())) {
            emailaddress = mEditEmail.getText().toString().trim();
        }
        String mobilePhoneNumber = mEditContactNumber.getText().toString();

        String deviceId = TokenManager.getInstance().getDeviceId();

        CreateUserRequest request = new CreateUserRequest(firstName, lastName, mobilePhoneNumber, password, emailaddress, invitationCode, deviceId);
        ((WelcomeActivity)mWelcomeActivity).mintlogEvent("Create Account Request - "+new Gson().toJson(request));
        nccApi.createUser(BuildConfig.APIGEE_URL+"/profile/new-driver", BuildConfig.APIGEE_API_KEY, request).enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                hideProgress();
                if(isAdded()) {
                    if (response != null && response.body() != null) {
                        Timber.d(response.body().toString());
                        CreateUserResponse responseModel = response.body();
                        ((WelcomeActivity)mWelcomeActivity).mintlogEvent("Create Account Response - "+new Gson().toJson(responseModel));
                            mEditor.putString(NccConstants.SIGNIN_VENDOR_ID, responseModel.getCredentials().getUser_claims().getFacility_id()).commit();
                            mEditor.putString(NccConstants.SIGNIN_USER_ID, responseModel.getCredentials().getUser_claims().getUser_id()).commit();
                            mEditor.putString(NccConstants.SIGNIN_USER_NAME, responseModel.getUser().getFirstName() + " " + responseModel.getUser().getLastName()).commit();
                            String roles = new Gson().toJson(responseModel.getUser().getRoles());
                            mEditor.putString(NccConstants.SIGNIN_USER_ROLE, roles).commit();

                        mEditor.putString(NccConstants.APIGEE_TOKEN, responseModel.getCredentials().getId_token()).commit();
                        mEditor.putString(NccConstants.FIREBASE_TOKEN, responseModel.getCredentials().getFirebase_token()).commit();
                        long expiresIn = (System.currentTimeMillis()) + ( responseModel.getCredentials().getExpires_in() * 1000);
                        expiresIn = expiresIn - 600000;
                        mEditor.putLong(NccConstants.APIGEE_TOKEN_EXPIRE, expiresIn).commit();
                        mEditor.putString(NccConstants.FIREBASE_REFRESH_TOKEN, responseModel.getCredentials().getRefresh_token()).commit();

                        TokenManager.getInstance().signinFirebase(mWelcomeActivity, responseModel.getCredentials().getFirebase_token(), expiresIn, new TokenManager.TokenReponseListener() {
                            @Override
                            public void onRefreshSuccess() {
                                if (isAdded()) {
                                    hideProgress();
                                    Intent intent = new Intent(mWelcomeActivity, HomeActivity.class);
                                    SparkLocationUpdateService.clearList();
                                    mWelcomeActivity.startActivity(intent);
                                    mWelcomeActivity.finish();
                                }
                            }

                            @Override
                            public void onRefreshFailure() {
                                hideProgress();
                                if (mWelcomeActivity != null && !mWelcomeActivity.isFinishing()) {
                                    Toast.makeText(mWelcomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        }, responseModel.getCredentials().getId_token());


                    } else if (response.errorBody() != null) {
                        try {
                            JSONObject errorResponse = new JSONObject(response.errorBody().string());
                            errorResponse.get("message");
                            if (errorResponse != null) {
                                if (errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                    ((WelcomeActivity)mWelcomeActivity).mintlogEvent("Create Account Api Error - "+errorResponse.get("message"));
                                    verificationCodeAlertDialog();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                ((WelcomeActivity)mWelcomeActivity).mintlogEvent("Create Account Api Error - "+t.getLocalizedMessage());
                hideProgress();
            }
        });

    }

    private void startHomeScreen() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        SparkLocationUpdateService.clearList();
        startActivity(intent);
        mWelcomeActivity.finish();
    }

}
