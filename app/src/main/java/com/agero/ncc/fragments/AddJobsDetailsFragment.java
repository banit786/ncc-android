package com.agero.ncc.fragments;

import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.DisablementLocation;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Status;
import com.agero.ncc.model.Vehicle;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AddJobsDetailsFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    public static JobDetail newJob;
    Unbinder unbinder;
    @BindView(R.id.text_add_jobs_title)
    TextView textAddJobTitle;
    @BindView(R.id.spinner_job_type)
    Spinner spinnerJobType;
    @BindView(R.id.spinner_service_type)
    Spinner spinnerServiceType;
    @BindView(R.id.spinner_state)
    Spinner spinnerState;
    @BindView(R.id.edit_disablement)
    EditText editDisablementAddress;
    @BindView(R.id.edit_city)
    EditText editCity;
    @BindView(R.id.edit_zip)
    EditText editZip;
    @BindView(R.id.radioButton_driver_vehicle_no)
    RadioButton rb_driverWithVehicleNo;
    @BindView(R.id.radioButton_driver_vehicle_yes)
    RadioButton rb_driverWithVehicleYes;
    @BindView(R.id.radioButton_coverage_no)
    RadioButton rb_coverageNo;
    @BindView(R.id.radioButton_coverage_yes)
    RadioButton rb_coverageYes;
    @BindView(R.id.radioButton_urgency_low)
    RadioButton rb_urgencyLow;
    @BindView(R.id.radioButton_urgency_med)
    RadioButton rb_urgencyMed;
    @BindView(R.id.radioButton_urgency_high)
    RadioButton rb_urgencyHigh;
    @BindView(R.id.spinner_passenger)
    Spinner mSpinnerPassenger;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    private boolean isAddressSetWithCurrentLocation;
    Vehicle vehicle;
    UserError mUserError;

    public AddJobsDetailsFragment() {
        // Intentionally empty
    }

    public static AddJobsDetailsFragment newInstance(boolean isAddNew) {
        AddJobsDetailsFragment fragment = new AddJobsDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(NccConstants.BUNDLE_KEY_ADD_NEW, isAddNew);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_job_details, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_job));
        mUserError = new UserError();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
            UUID myuuid = UUID.randomUUID();
            long highBits = myuuid.getMostSignificantBits();
            long lowBits = myuuid.getLeastSignificantBits();
            long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;

            newJob = new JobDetail();
            vehicle = new Vehicle();

            //newJob.setEta(90);

            Status currentStatus = new Status();
            currentStatus.setStatusCode("Assigned");

            newJob.setCurrentStatus(currentStatus);
            newJob.setPoNumber(String.valueOf(number));
            newJob.setDispatchId(String.valueOf(lowBits).replace("-", ""));
//            newJob.setDriverId(Integer.parseInt(mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")));

            textAddJobTitle.setText("PO# " + number);
        } else {
            List<String> listJobType = Arrays.asList(getResources().getStringArray(R.array.array_job_type));
            spinnerJobType.setSelection(listJobType.indexOf(newJob.getServiceTypeCode()));

            vehicle = newJob.getVehicle();
            List<String> listServiceType = Arrays.asList(getResources().getStringArray(R.array.array_serivce_type));
            spinnerServiceType.setSelection(listServiceType.indexOf(newJob.getServiceTypeCode()));

            List<String> listStates = Arrays.asList(getResources().getStringArray(R.array.array_states));
            spinnerState.setSelection(listStates.indexOf(newJob.getDisablementLocation().getState()));

            textAddJobTitle.setText("PO# " + newJob.getPoNumber());
            editDisablementAddress.setText(newJob.getDisablementLocation().getAddressLine1());
            editCity.setText(newJob.getDisablementLocation().getCity());
            editZip.setText(newJob.getDisablementLocation().getPostalCode());
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    getActivity(), R.array.array_vehicle_passenger, android.R.layout.simple_spinner_item);
            mSpinnerPassenger.setSelection(adapter.getPosition(""+newJob.getVehicle().getNumberOfPassengers()));
            rb_coverageNo.setChecked(newJob.getCoverage().equals("No") ? true : false);
            rb_coverageYes.setChecked(newJob.getCoverage().equals("Yes") ? true : false);
            if(newJob.getIsCustomerWithVehicle()){
                rb_driverWithVehicleYes.setChecked(true);
            }else{
                rb_driverWithVehicleNo.setChecked(true);
            }

        }
        return superView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_job_detail_save, R.id.radioButton_driver_vehicle_yes, R.id.radioButton_driver_vehicle_no, R.id.radioButton_coverage_no, R.id.radioButton_coverage_yes
            , R.id.radioButton_urgency_low, R.id.radioButton_urgency_med, R.id.radioButton_urgency_high, R.id.button_current_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_job_detail_save:
                if(Utils.isNetworkAvailable()){
                    if (editDisablementAddress.getText().toString().isEmpty() || editCity.getText().toString().isEmpty()) {
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Address fields shouldn't be empty"));
                    } else if (spinnerState.getSelectedItemPosition() == 0) {
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Please select one of the States"));
                    } else {
                        new GeoCodingDisablementTask().execute(editDisablementAddress.getText().toString() + "," +
                                editCity.getText().toString() + "," + spinnerState.getSelectedItem().toString());
                    }
                }else{
                    mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                }

                break;
            case R.id.radioButton_driver_vehicle_yes:
                rb_driverWithVehicleNo.setChecked(false);
                //newJob.isCustomerWithVehicle(true);
                break;
            case R.id.radioButton_driver_vehicle_no:
                rb_driverWithVehicleYes.setChecked(false);
                //newJob.isCustomerWithVehicle(false);
                break;
            case R.id.radioButton_coverage_no:
                rb_coverageYes.setChecked(false);
                newJob.setCoverage("No");
                break;
            case R.id.radioButton_coverage_yes:
                rb_coverageNo.setChecked(false);
                newJob.setCoverage("Yes");
                break;
            case R.id.radioButton_urgency_low:
                rb_urgencyHigh.setChecked(false);
                rb_urgencyMed.setChecked(false);
//                newJob.setUrgency("Low");
                break;
            case R.id.radioButton_urgency_med:
                rb_urgencyHigh.setChecked(false);
                rb_urgencyLow.setChecked(false);
//                newJob.getService().setUrgency("Medium");
                break;
            case R.id.radioButton_urgency_high:
                rb_urgencyLow.setChecked(false);
                rb_urgencyMed.setChecked(false);
//                newJob.getService().setUrgency("High");
                break;
            case R.id.button_current_location:
                getLastLocation();
                break;
        }

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            showProgress();
                            new ReverseGeoCodingTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        } else {
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Failed to get current location"));
                        }
                    }
                });
    }

    private class GeoCodingDisablementTask extends AsyncTask<String, Integer, Address> {

        @Override
        protected Address doInBackground(String... strings) {
            return Utils.getGeoCodedAddress(getActivity(), strings[0]);
        }

        @Override
        protected void onPostExecute(Address address) {
            if (address != null) {

//                if (!isAddressSetWithCurrentLocation) {
                DisablementLocation location = new DisablementLocation();
                location.setAddressLine1(editDisablementAddress.getText().toString());
                location.setCity(editCity.getText().toString());
                location.setState(spinnerState.getSelectedItem().toString());
                location.setPostalCode(editZip.getText().toString());
                location.setLatitude(address.getLatitude());
                location.setLongitude(address.getLongitude());
//                }
               // vehicle.setNumberOfPassengers(mSpinnerPassenger.getSelectedItem().);
                newJob.setVehicle(vehicle);
                if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
                    newJob.setDisablementLocation(location);
                    newJob.setServiceTypeCode(spinnerJobType.getSelectedItem().toString());
                    if (spinnerJobType.getSelectedItem().toString().startsWith("Tow")) {
                        mHomeActivity.push(AddTowDestinationFragment.newInstance(true), getString(R.string.title_add_tow));
                    } else {
                        mHomeActivity.push(AddVehicleDetailsFragment.newInstance(true), getString(R.string.title_add_vehicle));
                    }
                } else {
                    newJob.setDisablementLocation(location);
                    mHomeActivity.onBackPressed();
                }

            } else {
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.add_job_detail_enter_valid_address)));
            }
        }
    }

    private class ReverseGeoCodingTask extends AsyncTask<Double, Integer, Address> {

        @Override
        protected Address doInBackground(Double... latLng) {
            return Utils.getReverseGeoCodedAddress(getActivity(), latLng[0], latLng[1]);
        }

        @Override
        protected void onPostExecute(Address address) {
            hideProgress();
            DisablementLocation disablementLocation;
            if (address != null) {
                disablementLocation = new DisablementLocation();
                editDisablementAddress.setText(address.getAddressLine(0));
                editCity.setText(address.getLocality());
                editZip.setText(address.getPostalCode());
                try {
                    List<String> states = Arrays.asList(getResources().getStringArray(R.array.array_states));
                    spinnerState.setSelection(states.indexOf(Utils.getStatesAbbreviation(address.getAdminArea())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                disablementLocation.setAddressLine1(address.getAddressLine(0));
                disablementLocation.setCity(address.getLocality());
                disablementLocation.setState(address.getAdminArea());
                disablementLocation.setPostalCode(address.getPostalCode());
                disablementLocation.setLatitude(address.getLatitude());
                disablementLocation.setLongitude(address.getLongitude());

                newJob.setDisablementLocation(disablementLocation);
                isAddressSetWithCurrentLocation = true;
            } else {
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Failed to get current location"));
            }
        }

    }

}
