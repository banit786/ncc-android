package com.agero.ncc.fragments;


import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SendLegalDocumentDialogFragment extends DialogFragment {
    @BindView(R.id.image_sms)
    ImageView mImageSms;
    @BindView(R.id.edit_mobile_number)
    EditText mEditMobileNumber;
    @BindView(R.id.image_sms_send)
    ImageView mImageSmsSend;
    @BindView(R.id.image_mail)
    ImageView mImageMail;
    @BindView(R.id.edit_email)
    EditText mEditEmail;
    @BindView(R.id.image_mail_send)
    ImageView mImageMailSend;
    @BindView(R.id.text_no_thanks)
    TextView mTextNoThanks;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_send_legal_document, container);
        unbinder = ButterKnife.bind(this, v);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mEditEmail.addTextChangedListener(mTextWatcher);
        mImageMailSend.setEnabled(false);
        mTextNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

            }
        });

        mImageSmsSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent sendSMS = new Intent(Intent.ACTION_VIEW);
                    sendSMS.setData(Uri.parse("sms:" + mEditMobileNumber.getText().toString()));
                    startActivity(sendSMS);
            }
        });

        mImageMailSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendMail = new Intent(Intent.ACTION_VIEW);
                sendMail.setData(Uri.parse("mailto:"+mEditEmail.getText().toString()));
                startActivity(sendMail);
            }
        });
        return v;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(isValidEmail(mEditEmail.getText().toString().trim())){
                mImageMailSend.setImageResource(R.mipmap.send_blue);
                mImageMailSend.setEnabled(true);
            } else {
                mImageMailSend.setImageResource(R.mipmap.send_black);
                mImageMailSend.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
