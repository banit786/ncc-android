package com.agero.ncc.model

/**
 * Created by sdevabhaktuni on 9/6/17.
 */


data class Equipment(
        var facilityId: String? = null,
        @get:JvmName("getIsAvailable") @set:JvmName("setAvailable") var isAvailable: Boolean? = false,
        var dolly: Boolean? = false,
        var equipmentType: String? = null,
        var equipmentId: String? = null,
        var gps: Boolean? = false,
        var name: String? = null,
        var plate: String? = null,
        var plateState: String? = null,
        var winch: Boolean? = false,
        var quantity: Int? = 0
)

data class EquipmentResponse (var results: List<Equipment>? = null)
